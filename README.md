# Spring Boot, MongoDB, Angular Restful API Tutorial

Build a Fully-Fledged Todo App with Spring Boot & MongoDB in the Backend and Angular in the frontend.

#### Requirements

* Java - 1.8.x

* Maven - 3.x.x

* MongoDB - 3.x.x

#### Steps to Setup

1. Clone the application

    `git clone git@github.com:hendisantika/spring-boot-mongodb-angular.git`

2. Build and run the backend app using maven

    ```
    cd spring-boot-backend
    mvn package
    java -jar target/todoapp-1.0.0.jar
    ```

    Alternatively, you can run the app without packaging it using -

    `mvn clean spring-boot:run`

    The backend server will start at `http://localhost:8080`.

3. Run the frontend app using npm
    ```
    cd angular-frontend
    npm install

    npm start
    ```
    Frontend server will run on `http://localhost:4200`.
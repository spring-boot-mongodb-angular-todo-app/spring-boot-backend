package com.hendisantika.todoapp.repository;

import com.hendisantika.todoapp.model.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : todo-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/01/18
 * Time: 21.24
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface TodoRepository extends MongoRepository<Todo, String> {
}
